package com.nicolasrf.orderfoodapp;

import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nicolasrf.orderfoodapp.Common.Common;
import com.nicolasrf.orderfoodapp.Database.Database;
import com.nicolasrf.orderfoodapp.Helper.RecyclerItemTouchHelper;
import com.nicolasrf.orderfoodapp.Interface.RecyclerItemTouchHelperListener;
import com.nicolasrf.orderfoodapp.Model.Favorite;
import com.nicolasrf.orderfoodapp.Model.Food;
import com.nicolasrf.orderfoodapp.Model.Order;
import com.nicolasrf.orderfoodapp.ViewHolder.CartAdapter;
import com.nicolasrf.orderfoodapp.ViewHolder.CartViewHolder;
import com.nicolasrf.orderfoodapp.ViewHolder.FavoriteAdapter;
import com.nicolasrf.orderfoodapp.ViewHolder.FavoriteViewHolder;
import com.nicolasrf.orderfoodapp.ViewHolder.FoodViewHolder;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class FavoritesActivity extends AppCompatActivity implements RecyclerItemTouchHelperListener {

    FavoriteAdapter adapter;

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    RelativeLayout rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        rootLayout = findViewById(R.id.root_layout);

        //Load menu
        recyclerView = findViewById(R.id.favorites_recycler);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(recyclerView.getContext(),
                R.anim.layout_from_left);
        controller.setDelay(0.1f);
        recyclerView.setLayoutAnimation(controller);

        //Swipe to delete
        ItemTouchHelper.SimpleCallback simpleCallback = new RecyclerItemTouchHelper(0,ItemTouchHelper.LEFT,this);
        new ItemTouchHelper(simpleCallback).attachToRecyclerView(recyclerView);

        //Load favorites
        adapter = new FavoriteAdapter(this, new Database(this).getAllFavorites(Common.currentUser.getPhone()));
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if(viewHolder instanceof FavoriteViewHolder){

            String name = ((FavoriteAdapter)recyclerView.getAdapter()).getItem(viewHolder.getAdapterPosition()).getFoodName();

            final Favorite deleteItem = ((FavoriteAdapter)recyclerView.getAdapter()).getItem(viewHolder.getAdapterPosition());
            final int deleteIndex = viewHolder.getAdapterPosition();

            adapter.removeItem(deleteIndex);
            new Database(getBaseContext()).removeFromFavorites(deleteItem.getFoodId(), Common.currentUser.getPhone());

            //Make snackbar
            Snackbar snackbar = Snackbar.make(rootLayout, name + " removed from favorites!", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapter.restoreItem(deleteItem,deleteIndex);
                    new Database(getBaseContext()).addToFavorites(deleteItem);

                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }
}
