package com.nicolasrf.orderfoodapp.Interface;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Nicolas on 30/03/2018.
 */

public interface RecyclerItemTouchHelperListener {
    void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
}
