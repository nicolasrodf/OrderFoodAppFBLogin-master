package com.nicolasrf.orderfoodapp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.nicolasrf.orderfoodapp.Common.Common;
import com.nicolasrf.orderfoodapp.Interface.ItemClickListener;
import com.nicolasrf.orderfoodapp.Model.Food;
import com.nicolasrf.orderfoodapp.Model.Request;
import com.nicolasrf.orderfoodapp.ViewHolder.FoodViewHolder;
import com.nicolasrf.orderfoodapp.ViewHolder.OrderViewHolder;

import java.util.Collections;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OrderStatusActivity extends AppCompatActivity {

    public RecyclerView recyclerView;
    public RecyclerView.LayoutManager layoutManager;

    FirebaseRecyclerAdapter<Request, OrderViewHolder> adapter;

    FirebaseDatabase database;
    DatabaseReference requests;

    //Press crtl+O
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);

        //Firebase
        database = FirebaseDatabase.getInstance();
        requests = database.getReference("Requests");

        recyclerView = findViewById(R.id.list_orders_recycler);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        if(Common.isConnectedToInternet(this)) {
            //If we start OrderStatusActivity from HomeActivity
            //We will not put any extra, so we just loadOrder by phone from Common.
            if (getIntent() != null) { //se cambio en vez de getIntent()!=null (getIntent().getExtras() == null) y se resolvio el problema de abrir la notificacion... :/
                loadOrders(Common.currentUser.getPhone()); //load all orders
            } else {
                if(getIntent().getStringExtra("userPhone") == null) {
                    loadOrders(Common.currentUser.getPhone()); //load all orders

                } else {
                    loadOrders(getIntent().getStringExtra("userPhone"));
                }
            }
        } else {
            Toast.makeText(this, "Check your internet connection!!", Toast.LENGTH_SHORT).show();
        }

    }

    private void loadOrders(String phone) {
        //
        Query getOrderByUser = requests.orderByChild("phone")
                .equalTo(phone);
        FirebaseRecyclerOptions<Request> orderOptions = new FirebaseRecyclerOptions.Builder<Request>()
                .setQuery(getOrderByUser,Request.class)
                .build();
        adapter = new FirebaseRecyclerAdapter<Request, OrderViewHolder>(orderOptions) {
            @Override
            protected void onBindViewHolder(@NonNull OrderViewHolder viewHolder, final int position, @NonNull Request model) {
                viewHolder.orderIdTextView.setText(adapter.getRef(position).getKey());
                viewHolder.orderStatusTextView.setText(Common.convertCodeToStatus(model.getStatus()));
                viewHolder.orderAddressTextView.setText(model.getAddress());
                viewHolder.orderPhoneTextView.setText(model.getPhone());
                viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(adapter.getItem(position).getStatus().equals("0")) //si order esta en "placed"
                            deleteOrder(adapter.getRef(position).getKey()); //de esa orden
                        else
                            Toast.makeText(OrderStatusActivity.this, "You cannot delete this Order!", Toast.LENGTH_SHORT).show();
                    }
                });

                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //just implement it to fix Crash when click to this item
                        Common.currentKey = adapter.getRef(position).getKey();
                        //Todo. TrackingOrder is not working. It will replaced by Order detail.
                        //startActivity(new Intent(OrderStatusActivity.this, TrackingOrderActivity.class));
                    }
                });
            }

            @Override
            public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.order_layout,parent,false);
                return new OrderViewHolder(itemView);
            }
        };

        adapter.startListening();
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
    }

    private void deleteOrder(final String key) {
        requests.child(key)
                .removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(OrderStatusActivity.this, new StringBuilder("Order")
                        .append(key)
                        .append(" has been deleted!").toString(), Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(OrderStatusActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    //crtkl+O
    @Override
    protected void onResume() {
        super.onResume();
        loadOrders(Common.currentUser.getPhone());
        //Fix click back
        if(adapter != null){
            adapter.startListening();
        }
    }
}
