package com.nicolasrf.orderfoodapp.Service;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.nicolasrf.orderfoodapp.Common.Common;
import com.nicolasrf.orderfoodapp.Model.Token;

/**
 * Created by Nicolas on 22/03/2018.
 */

public class MyFirebaseIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if(Common.currentUser != null) {
            updateToken(refreshedToken); //When have refresh tokem we need update to our Realtime database
        }

    }

    private void updateToken(String refreshedToken) {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference tokens = db.getReference(Common.token_table);

        Token token = new Token(refreshedToken,false); //false because this token send from Clien app
        tokens.child(Common.currentUser.getPhone()).setValue(token);

    }
}
