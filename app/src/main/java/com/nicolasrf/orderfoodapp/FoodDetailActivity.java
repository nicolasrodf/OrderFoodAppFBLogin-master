package com.nicolasrf.orderfoodapp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nicolasrf.orderfoodapp.Common.Common;
import com.nicolasrf.orderfoodapp.Database.Database;
import com.nicolasrf.orderfoodapp.Model.Food;
import com.nicolasrf.orderfoodapp.Model.Order;
import com.nicolasrf.orderfoodapp.Model.Rating;
import com.squareup.picasso.Picasso;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

import java.util.Arrays;

import info.hoang8f.widget.FButton;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FoodDetailActivity extends AppCompatActivity implements RatingDialogListener{

    TextView foodNameTextView, foodDescTextView, foodPriceTextView;
    ImageView foodImageView;
    CollapsingToolbarLayout collapsingToolbarLayout;
    CounterFab cartButtonFab;
    FloatingActionButton ratingBarFab;
    ElegantNumberButton numberButton;
    RatingBar ratingBar;

    FirebaseDatabase database;
    DatabaseReference foods;
    DatabaseReference ratingTable;

    String foodId = "";
    Food currentFood;

    FButton showCommentsButton;

    //Press crtl+O
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Note: add this code before setContentView method
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/AmaticSC-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        setContentView(R.layout.activity_food_detail);

        //Init firebase
        database = FirebaseDatabase.getInstance();
        foods = database.getReference("Foods");
        ratingTable = database.getReference("Rating");

        //INit view
        numberButton = findViewById(R.id.number_button);
        cartButtonFab = findViewById(R.id.cart_button_fab);
        foodDescTextView = findViewById(R.id.food_desc_text_view);
        foodNameTextView = findViewById(R.id.food_name_text_view);
        foodPriceTextView = findViewById(R.id.food_price_text_view);
        foodImageView = findViewById(R.id.food_image_view);
        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        ratingBarFab = findViewById(R.id.rating_button_fab);
        ratingBar = findViewById(R.id.comment_rating_bar);

        showCommentsButton = findViewById(R.id.show_comments_button);
        showCommentsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FoodDetailActivity.this, ShowCommentActivity.class);
                intent.putExtra(Common.INTENT_FOOD_ID, foodId);
                startActivity(intent);
            }
        });


        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);

        cartButtonFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Database(getBaseContext()).addToCart(new Order(
                        Common.currentUser.getPhone(),
                        foodId,
                        currentFood.getName(),
                        numberButton.getNumber(),
                        currentFood.getPrice(),
                        currentFood.getDiscount(),
                        currentFood.getImage()
                ));

                Toast.makeText(FoodDetailActivity.this, "Added to Cart.", Toast.LENGTH_SHORT).show();
            }
        });

        cartButtonFab.setCount(new Database(this).getCountCart(Common.currentUser.getPhone()));

        ratingBarFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRatingDialog();
            }
        });

        //gfet foodId from intent
        if(getIntent() != null){
            foodId = getIntent().getStringExtra("foodId");
            if(!foodId.isEmpty() && foodId != null){
                //checl internet connection and load foods detail
                if(Common.isConnectedToInternet(this)) {
                    getRatingFood(foodId);
                    getDetailFood(foodId);
                } else {
                    Toast.makeText(FoodDetailActivity.this, "Please check your connection!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void getRatingFood(String foodId) {

        Query foodRating = ratingTable.orderByChild("foodId").equalTo(foodId);
        foodRating.addValueEventListener(new ValueEventListener() {

            int count = 0, sum = 0;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot postSnapshot:dataSnapshot.getChildren()){

                    Rating item = postSnapshot.getValue(Rating.class);
                    if (item != null) {
                        sum+=Integer.parseInt(item.getRateValue());
                    }
                    count++;
                }

                if(count != 0){
                    float average = sum/count;
                    ratingBar.setRating(average);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showRatingDialog() {
        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                .setNoteDescriptions(Arrays.asList("Very bad","Not good", "Quite ok", "Very good", "Excellent"))
                .setDefaultRating(1)
                .setTitle("Rate this foods")
                .setDescription("Please select some stars and give your feedback")
                .setTitleTextColor(R.color.colorPrimary)
                .setDescriptionTextColor(R.color.colorPrimary)
                .setHint("Please write your comment here...")
                .setHintTextColor(R.color.colorAccent)
                .setCommentTextColor(android.R.color.white)
                .setCommentBackgroundColor(R.color.colorPrimaryDark)
                .setWindowAnimation(R.style.RatingDialogFadeAnim)
                .create(FoodDetailActivity.this)
                .show();
    }

    private void getDetailFood(String foodId) {

        foods.child(foodId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                currentFood = dataSnapshot.getValue(Food.class);

                //Set Image
                Picasso.with(getBaseContext()).load(currentFood.getImage())
                        .into(foodImageView);

                collapsingToolbarLayout.setTitle(currentFood.getName());
                foodPriceTextView.setText(currentFood.getPrice());
                foodNameTextView.setText(currentFood.getName());
                foodDescTextView.setText(currentFood.getDescription());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onPositiveButtonClicked(int value, String comments) {
        //Get rating
        final Rating rating = new Rating(
                Common.currentUser.getPhone(),
                foodId,
                String.valueOf(value),
                comments);

        //Fix use can rate multiple times
        ratingTable.push()
                .setValue(rating)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(FoodDetailActivity.this, "Thank you for submit rating !!!", Toast.LENGTH_SHORT).show();
                    }
                });

        //upload to firebase
        /*
        ratingTable.child(Common.currentUser.getPhone())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.child(Common.currentUser.getPhone()).exists()){
                            //remove old value (you can delete or let it be - useless function :D)
                            ratingTable.child(Common.currentUser.getPhone()).removeValue();
                            //Update new value
                            ratingTable.child(Common.currentUser.getPhone()).setValue(rating);
                        } else {
                            //Update new value
                            ratingTable.child(Common.currentUser.getPhone()).setValue(rating);
                        }

                        Toast.makeText(FoodDetailActivity.this, "Thank you for submit rating !!!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                */
    }

    @Override
    public void onNegativeButtonClicked() {

    }
}
