package com.nicolasrf.orderfoodapp.Common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;

import com.nicolasrf.orderfoodapp.Model.User;
import com.nicolasrf.orderfoodapp.Remote.GoogleRetrofitClient;
import com.nicolasrf.orderfoodapp.Remote.FCMRetrofitClient;
import com.nicolasrf.orderfoodapp.Remote.APIService;
import com.nicolasrf.orderfoodapp.Remote.IGoogleService;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Nicolas on 18/03/2018.
 */

public class Common {

    public static String topicName ="News";

    public static User currentUser;
    public static String currentKey;

    public static String PHONE_TEXT = "userPhone";
    public static final String INTENT_FOOD_ID = "foodId";

    public static final String DELETE = "Delete";
    public static final String USER_KEY = "User";
    public static final String PWD_KEY = "Password";
    public static final String token_table = "Tokens";

    /* for FCM service**/
    public static final String fcmUrl = "https://fcm.googleapis.com/";
    public static APIService getFCMService(){
        return FCMRetrofitClient.getClient(fcmUrl).create(APIService.class);
    }
    /**/

    /* for Google API service**/
    public static final String googleAPIUrl = "https://maps.googleapis.com/";
    public static IGoogleService getGoogleMapsAPI(){
        return GoogleRetrofitClient.getGoogleClient(googleAPIUrl).create(IGoogleService.class);
    }
    /**/

    public static String convertCodeToStatus(String code){
        if(code.equals("0")){
            return "Placed";
        } else if(code.equals("1")) {
            return "On my way";
        } else if(code.equals("2")){
            return "Shipping";
        } else {
            return "Shipped";
        }
    }

    public static boolean isConnectedToInternet(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager != null){
            NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
            if(info != null){
                for(int i=0; i<info.length ; i++){
                    if(info[i].getState() == NetworkInfo.State.CONNECTED){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    //his funtion will convert concurrencyt base on Locale
    public static BigDecimal formatConcurrency(String amount, Locale locale) throws ParseException, java.text.ParseException {
        NumberFormat format = NumberFormat.getCurrencyInstance(locale);
        if(format instanceof DecimalFormat){
            ((DecimalFormat)format).setParseBigDecimal(true);
        }
        return (BigDecimal)format.parse(amount.replace("[^\\d.,]",""));
    }
}
