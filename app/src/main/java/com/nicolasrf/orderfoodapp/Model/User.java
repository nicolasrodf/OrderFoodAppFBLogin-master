package com.nicolasrf.orderfoodapp.Model;

/**
 * Created by Nicolas on 17/03/2018.
 */

public class User {

    private String name, password, phone, isStaff, secureCode, homeAddress;
    Object balance; //para manejarlo mejor.

    public User() {
    }


    public User(String name, String password, String secureCode, String homeAddress) {
        this.name = name;
        this.password = password;
        isStaff = "false"; //default
        this.secureCode = secureCode;
        this.homeAddress = homeAddress;
    }

    public Object getBalance() {
        return balance;
    }

    public void setBalance(Object balance) {
        this.balance = balance;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getSecureCode() {
        return secureCode;
    }

    public void setSecureCode(String secureCode) {
        this.secureCode = secureCode;
    }

    public String getIsStaff() {
        return isStaff;
    }

    public void setIsStaff(String isStaff) {
        this.isStaff = isStaff;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
