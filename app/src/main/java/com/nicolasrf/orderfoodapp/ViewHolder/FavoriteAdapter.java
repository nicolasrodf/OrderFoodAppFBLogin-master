package com.nicolasrf.orderfoodapp.ViewHolder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nicolasrf.orderfoodapp.Common.Common;
import com.nicolasrf.orderfoodapp.Database.Database;
import com.nicolasrf.orderfoodapp.FoodDetailActivity;
import com.nicolasrf.orderfoodapp.FoodListActivity;
import com.nicolasrf.orderfoodapp.Interface.ItemClickListener;
import com.nicolasrf.orderfoodapp.Model.Favorite;
import com.nicolasrf.orderfoodapp.Model.Order;
import com.nicolasrf.orderfoodapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Nicolas on 30/03/2018.
 */

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteViewHolder> {

    private Context context;
    private List<Favorite> favoriteList;

    public FavoriteAdapter(Context context, List<Favorite> favoriteList) {
        this.context = context;
        this.favoriteList = favoriteList;
    }

    @Override
    public FavoriteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.favorite_item,parent,false);
        return  new FavoriteViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(FavoriteViewHolder viewHolder, final int position) {

        viewHolder.favoriteNameTextView.setText(favoriteList.get(position).getFoodName());
        viewHolder.favoritePriceTextView.setText(String.format("$ %s", favoriteList.get(position).getFoodPrice()));
        Picasso.with(context)
                .load(favoriteList.get(position).getFoodImage())
                .into(viewHolder.foodImageView);

        //Quick orders

        viewHolder.quickCartImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFoodExists = new Database(context).checkIfFoodExists(favoriteList.get(position).getFoodId(),Common.currentUser.getPhone());

                if(!isFoodExists) {
                    new Database(context).addToCart(new Order(
                            Common.currentUser.getPhone(),
                            favoriteList.get(position).getFoodId(),
                            favoriteList.get(position).getFoodName(),
                            "1",
                            favoriteList.get(position).getFoodPrice(),
                            favoriteList.get(position).getFoodDiscount(),
                            favoriteList.get(position).getFoodImage()
                    ));

                } else {

                    new Database(context).increaseCart(Common.currentUser.getPhone(), favoriteList.get(position).getFoodId());
                }
                Toast.makeText(context, "Added to Cart.", Toast.LENGTH_SHORT).show();
            }
        });



        viewHolder.setItemClickListener(new ItemClickListener(){
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                //Start new activiyt
                Intent intent = new Intent(context, FoodDetailActivity.class);
                intent.putExtra("foodId", favoriteList.get(position).getFoodId()); //send foodId to new activity
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return favoriteList.size();
    }

    public void removeItem(int position){
        favoriteList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(Favorite item, int position){
        favoriteList.add(position, item);
        notifyItemInserted(position);
    }

    public Favorite getItem(int position){
        return favoriteList.get(position);
    }


}
