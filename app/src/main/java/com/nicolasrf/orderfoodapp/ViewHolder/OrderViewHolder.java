package com.nicolasrf.orderfoodapp.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nicolasrf.orderfoodapp.Interface.ItemClickListener;
import com.nicolasrf.orderfoodapp.R;

/**
 * Created by Nicolas on 19/03/2018.
 */

public class OrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView orderIdTextView, orderStatusTextView, orderPhoneTextView, orderAddressTextView;
    public ImageView deleteButton;

    private ItemClickListener itemClickListener;

    public OrderViewHolder(View itemView) {
        super(itemView);

        orderAddressTextView = itemView.findViewById(R.id.order_address_text_view);
        orderStatusTextView = itemView.findViewById(R.id.order_status_text_view);
        orderPhoneTextView = itemView.findViewById(R.id.order_phone_text_view);
        orderIdTextView = itemView.findViewById(R.id.order_id_text_view);
        deleteButton = itemView.findViewById(R.id.delete_button);

        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v, getAdapterPosition(), false);

    }
}
